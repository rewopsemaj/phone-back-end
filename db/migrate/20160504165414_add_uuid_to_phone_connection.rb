class AddUuidToPhoneConnection < ActiveRecord::Migration
  def change
    add_column :phone_connections, :uuid, :string
  end
end
