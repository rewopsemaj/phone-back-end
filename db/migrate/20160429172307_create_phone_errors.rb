class CreatePhoneErrors < ActiveRecord::Migration
  def change
    create_table :phone_errors do |t|
      t.references :phone_connection, index: true
      t.timestamp :reported_time

      t.timestamps
    end
  end
end
