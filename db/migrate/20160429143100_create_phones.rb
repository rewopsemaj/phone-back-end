class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.references :user, index: true
      t.decimal :latitude, :precision => 11, :scale => 8
      t.decimal :longitude, :precision => 11, :scale => 8
      t.timestamp :gps_poll_time
      t.timestamp :gps_sent_time
      t.integer :num_clients
      t.integer :max_clients

      t.timestamps
    end
  end
end
