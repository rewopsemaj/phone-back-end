class AddInSubGraphToPhone < ActiveRecord::Migration
  def change
    add_column :phones, :in_sub_graph, :boolean, default: false
  end
end
