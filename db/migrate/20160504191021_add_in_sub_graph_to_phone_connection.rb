class AddInSubGraphToPhoneConnection < ActiveRecord::Migration
  def change
    add_column :phone_connections, :in_sub_graph, :boolean, default: false
  end
end
