class RemoveInSubGraphFromPhone < ActiveRecord::Migration
  def change
    remove_column :phones, :in_sub_graph, :boolean
  end
end
