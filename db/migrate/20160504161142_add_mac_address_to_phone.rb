class AddMacAddressToPhone < ActiveRecord::Migration
  def change
    add_column :phones, :mac_address, :string, unique: true
    add_index :phones, :mac_address
  end
end
