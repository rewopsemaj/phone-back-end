class CreatePhoneConnections < ActiveRecord::Migration
  def change
    create_table :phone_connections do |t|
      t.integer :client_id
      t.integer :server_id

      t.timestamps
    end
  end
end
