# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160506110215) do

  create_table "phone_connections", force: true do |t|
    t.integer  "client_id"
    t.integer  "server_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uuid"
    t.boolean  "in_sub_graph", default: false
  end

  create_table "phone_errors", force: true do |t|
    t.integer  "phone_connection_id"
    t.datetime "reported_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "phone_errors", ["phone_connection_id"], name: "index_phone_errors_on_phone_connection_id", using: :btree

  create_table "phones", force: true do |t|
    t.integer  "user_id"
    t.decimal  "latitude",      precision: 11, scale: 8
    t.decimal  "longitude",     precision: 11, scale: 8
    t.datetime "gps_poll_time"
    t.datetime "gps_sent_time"
    t.integer  "num_clients"
    t.integer  "max_clients"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "mac_address"
  end

  add_index "phones", ["mac_address"], name: "index_phones_on_mac_address", using: :btree
  add_index "phones", ["user_id"], name: "index_phones_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token",   limit: 30
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
