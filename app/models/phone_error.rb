class PhoneError < ActiveRecord::Base
  belongs_to :phone_connection
  after_save :check_errors

  validates :reported_time, presence: true

  ERROR_THRESHOLD = 2

  def as_json(options={})
    super(except: [:phone_connection_id, :created_at], include: [phone_connection: {only: [], methods: [:client_mac_address, :server_mac_address, :distance]}])
  end

  #
  # Get errors for which the server is the current phone
  # If the num errors is above a threshold then delete server (and corresponding objects)
  # Run graph algorithm on phones within 2 degrees of server
  #
  def check_errors
    server = phone_connection.server
    phone_errors = server.phone_errors
    # TODO: Check value of ERROR_THRESHOLD
    if phone_errors.count > ERROR_THRESHOLD
      phones_within_two_degrees = Phone.within_two_degrees_of(server)
      server.delete_connections
      server.delete_errors
      server.destroy
      Phone.graph_algorithm(phones_within_two_degrees)
    end
  end
end