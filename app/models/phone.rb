class Phone < ActiveRecord::Base
  belongs_to :user

  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :gps_poll_time, presence: true
  validates :gps_sent_time, presence: true
  validates :num_clients, presence: true
  validates :max_clients, presence: true
  validates :mac_address, presence: true

  def phone_connections
    PhoneConnection.where('client_id = ? OR server_id = ?', self.id, self.id)
  end

  def phone_errors
    phone_connection_ids = phone_connections.map{ |phone_connection| phone_connection.id }
    PhoneError.where('phone_connection_id IN (?)',phone_connection_ids)
  end

  def as_json(options={})
    # may be unnecessary
    super(:except => [:created_at, :updated_at], methods: [:updated_at_timestamp])
  end

  def gps_poll_time=(timestamp)
    super(Time.at(timestamp))
  end

  def gps_poll_time
    super.to_i
  end

  def gps_sent_time=(timestamp)
    super(Time.at(timestamp))
  end

  def gps_sent_time
    super.to_i
  end

  def updated_at_timestamp
    updated_at.to_i
  end

  def delete_connections
    phone_connections.each(&:destroy)
  end

  def delete_errors
    phone_errors.each(&:destroy)
  end

  ######################
  ## ALGORITHM METHODS
  ######################

  def client_connections
    PhoneConnection.where('server_id = ?', self.id)
  end

  def clients
    client_connections.map(&:client)
  end

  # return all phones within two degrees of given phone
  def self.within_two_degrees_of(phone)
    phone_clients = phone.clients
    phone_clients + phone_clients.map(&:clients).flatten
  end

  # Run Kruskal's algorithm on the PhoneConnections to get minimum spanning tree (nodes in MST have in_sub_graph = true)
  def self.graph_algorithm(phones)
    chosen_connections = []
    phone_nodes = {}
    phones.each { |phone| phone_nodes[phone.id] = DisjointSetNode.new }
    puts phone_nodes
    PhoneConnection.all.sort { |x,y| x.distance <=> y.distance }.each do |phone_connection|
      puts phone_connection
      if phone_nodes[phone_connection.client_id].find_set != phone_nodes[phone_connection.server_id].find_set
        chosen_connections << phone_connection
        phone_nodes[phone_connection.client_id].union(phone_nodes[phone_connection.server_id])
      end
    end
    chosen_connections.each { |connection| connection.in_sub_graph = true; connection.save}
  end

  def run_graph_algorithm
    Phone.graph_algorithm(Phone.all)
  end

  #
  # def degree
  #   phone_connections.count
  # end
  #
  # def run_graph_algorithm
  #   Phone.greedy_algorithm(self)
  #   #Phone.graph_algorithm(Phone.within_two_degrees_of(self))
  # end
  #
  # def client_connections
  #   PhoneConnection.where('server_id = ?', self.id)
  # end
  #
  # def clients
  #   client_connections.map(&:client)
  # end
  #
  # def server_connections
  #   PhoneConnection.where('client_id = ?', self.id)
  # end
  #
  # def servers
  #   server_connections.map(&:server)
  # end
  #
  # def self.greedy_algorithm(phone)
  #   minimum_degree = Integer::MAX
  #   chosen_connections = []
  #   phone.client_connections.each do |client_connection|
  #     client_connection.in_sub_graph = false
  #     client_degree = client_connection.client.degree
  #     if client_degree < minimum_degree
  #       chosen_connections = [client_connection]
  #     elsif client_degree == minimum_degree
  #       chosen_connections << client_connection
  #     end
  #   end
  #   phone.server_connections.each do |server_connection|
  #     server_connection.in_sub_graph = false
  #     server_degree = server_connection.server.degree
  #     if server_degree < minimum_degree
  #       chosen_connections = [server_connection]
  #     elsif server_degree == minimum_degree
  #       chosen_connections << server_connection
  #     end
  #   end
  #   chosen_connections.each { |connection| connection.in_sub_graph = true; connection.save }
  # end
  #
  # # return all phones within two degrees of given phone
  # def self.within_two_degrees_of(phone)
  #   phone_clients = phone.clients
  #   phone_clients + phone_clients.map(&:clients).flatten
  # end
  #
  # def self.graph_algorithm(phones)
  #   phone_connections = PhoneConnection.all
  #   phone_connections.each{ |phone_connection| phone_connection.in_sub_graph = false }
  #   ## richard's code here
  # end
end
