class PhoneConnection < ActiveRecord::Base
  belongs_to :client, :class_name => 'Phone', :primary_key => 'id'
  belongs_to :server, :class_name => 'Phone', :primary_key => 'id'
  # No dependent: :destroy because of difficulty updating PhoneConnections via nested json data
  has_many :phone_errors

  # There must be no more than one PhoneConnection with the same client_id and server_id
  validates :client, uniqueness: { scope: :server }
  validates :client_id, presence: true
  validates :server_id, presence: true
  validate :set_uuid

  def dx
    (client.latitude - server.latitude).abs
  end

  def dy
    (client.longitude - server.longitude).abs
  end

  def distance
    distance_between(client.latitude, client.longitude, server.latitude, server.longitude)
  end

  def client_mac_address
    client.mac_address
  end

  def server_mac_address
    server.mac_address
  end

  def as_json(options={})
    # use :include => [:client, :server] for objects
    # use :only => [:client_id, :server_id] for ids
    super(only: [:uuid, :in_sub_graph], methods: [:distance, :server_mac_address, :client_mac_address])
  end

  private
    def distance_between(lat1, lon1, lat2, lon2)
      rad_per_deg = Math::PI / 180
      rm = 6371000 # Earth radius in meters

      lat1_rad, lat2_rad = lat1 * rad_per_deg, lat2 * rad_per_deg
      lon1_rad, lon2_rad = lon1 * rad_per_deg, lon2 * rad_per_deg

      a = Math.sin((lat2_rad - lat1_rad) / 2) ** 2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin((lon2_rad - lon1_rad) / 2) ** 2
      c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1 - a))

      rm * c # Delta in meters
    end

    def set_uuid
      if self.uuid.blank?
        self.uuid = SecureRandom.uuid
      end
    end
end
