class PhoneErrorsController < ApplicationController
  before_action :token_authenticate
  before_action :set_phone_error, only: [:show, :update, :destroy]

  # GET /phone_errors
  def index
    @phone_errors = PhoneError.all
    render json: @phone_errors
  end

  # GET /phone_errors/1
  def show
    render json: @phone_error
  end

  # POST /phone_errors
  def create
    @phone_error = PhoneError.new(phone_error_params.except(:server_mac_address,:client_mac_address))
    success = set_phone_connection_id(@phone_error)

    respond_to do |format|
      if success && @phone_error.save
        format.json { render json: @phone_error, status: :created, location: @phone_error }
      else
        format.json { render json: @phone_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phone_errors/1
  def update
    respond_to do |format|
      if @phone_error.update(phone_error_params.except(:server_mac_address, :client_mac_address))
        format.json { head :no_content }
      else
        format.json { render json: @phone_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phone_errors/1
  def destroy
    @phone_error.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def set_phone_error
      @phone_error = PhoneError.find(params[:id])
    end

    def phone_error_params
      params.require(:phone_error).permit(:reported_time,:server_mac_address,:client_mac_address)
    end

    def set_phone_connection_id(phone_error)
      client = Phone.find_by_mac_address(phone_error_params[:client_mac_address])
      server = Phone.find_by_mac_address(phone_error_params[:server_mac_address])
      if client.blank?
        phone_error.errors.add(:base, 'No Phone found for client_mac_address')
        return false
      end
      if server.blank?
        phone_error.errors.add(:base, 'No Phone found for server_mac_address')
        return false
      end
      phone_connection = PhoneConnection.where('client_id = ? AND server_id = ?', client.id, server.id).first
      if phone_connection.blank?
        phone_error.errors.add(:base, 'No PhoneConnection found for client_id and server_id.')
        return false
      else
        phone_error.phone_connection_id = phone_connection.id
        return true
      end
    end
end