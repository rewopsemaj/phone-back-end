class PhonesController < ApplicationController
  before_action :token_authenticate
  before_action :set_phone, only: [:show, :update, :destroy]

  # GET /phones
  def index
    @phones = Phone.all
    render json: @phones
  end

  # GET /phones/1
  def show
    render json: @phone
  end

  # POST /phones
  def create
    @phone = Phone.new(phone_params)
    @phone.user = current_user

    respond_to do |format|
      if @phone.save && create_phone_connections(@phone)
        @phone.run_graph_algorithm
        format.json { render json: @phone, status: :created, location: @phone }
      else
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phones/1
  def update
    @phone.delete_connections
    @phone.delete_errors

    respond_to do |format|
      if @phone.update(phone_params) && create_phone_connections(@phone)
        @phone.run_graph_algorithm
        format.json { head :no_content }
      else
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phones/1
  def destroy
    @phone.delete_connections
    @phone.delete_errors
    @phone.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def set_phone
      @phone = Phone.find(params[:id])
    end

    def phone_params
      params.require(:phone).permit(:latitude, :longitude, :gps_poll_time, :gps_sent_time, :num_clients, :max_clients, :mac_address)
    end

    def phone_connections_params
      params.require(:phone).permit(phone_connections: [:server_mac_address])
    end

    # Creates PhoneConnection objects for data passed in. If one fails to save then rollback.
    def create_phone_connections(phone)
      if phone_connections_params[:phone_connections].blank?
        return true
      end
      created_phone_connections = []
      phone_connections_params[:phone_connections].each do |phone_connection_params|
        phone_connection = PhoneConnection.new
        phone_connection.server = Phone.find_by_mac_address(phone_connection_params[:server_mac_address])
        phone_connection.client = phone
        if phone_connection.save
          created_phone_connections << phone_connection
        else
          phone_connection.errors.each { |attr,error| phone.errors.add(:base, "PhoneConnection: #{attr} #{error}") }
          created_phone_connections.each(&:destroy)
          return false
        end
      end
      true
    end
end
