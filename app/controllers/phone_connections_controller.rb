class PhoneConnectionsController < ApplicationController
  before_action :token_authenticate
  before_action :set_phone_connection, only: [:show, :destroy]

  # GET /phone_connections
  def index
    @phone_connections = PhoneConnection.all
    render json: @phone_connections
  end

  # GET /phone_connections/1
  def show
    render json: @phone_connection
  end

  # # POST /phone_connections
  # def create
  #   success, errors, phone_connections = create_phone_connections
  #   respond_to do |format|
  #     if success
  #       #TODO: How should we run the graph algorithm (all Phones or phones with new PhoneConnections)?
  #       format.json { render json: phone_connections, status: :created }
  #     else
  #       format.json { render json: errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /phone_connections/1
  def destroy
    @phone_connection.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def set_phone_connection
      @phone_connection = PhoneConnection.find(params[:id])
    end

    def phone_connection_params
      params.permit(phone_connections: [:server_mac_address, :client_mac_address])
    end

    def create_phone_connections
      created_phone_connections = []
      if params[:phone_connections].blank?
        return true, [], []
      end
      params[:phone_connections].each do |phone_connections_params|
        phone_connection = PhoneConnection.new
        phone_connection.client = Phone.find_by_mac_address(phone_connections_params[:client_mac_address])
        phone_connection.server = Phone.find_by_mac_address(phone_connections_params[:server_mac_address])
        if phone_connection.save
          created_phone_connections << phone_connection
        else
          created_phone_connections.each(&:destroy)
          return false, phone_connection.errors, []
        end
      end
      return true, [], created_phone_connections
    end
end
