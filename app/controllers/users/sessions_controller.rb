class Users::SessionsController < Devise::SessionsController
  respond_to :json
  skip_before_filter :verify_signed_out_user
  skip_before_filter :require_no_authentication
# before_filter :configure_sign_in_params, only: [:create]

  def new
    render status: 200, json: { message: 'Please log in' }
  end

  # POST /users/sign_in
  def create
    respond_to do |format|
      format.json {
        resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure", store: false)
        sign_in(resource_name,resource)
        current_user.reset_authentication_token
        render status: 200, json: { success: true, authentication_token: current_user.authentication_token }
      }
    end
  end

  # DELETE /users/sign_out
  def destroy
    resource = User.find_by_authentication_token(params[:auth_token]||request.headers['X-AUTH-TOKEN'])
    resource.authentication_token = nil
    resource.save
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    render status: 200, json: { message: 'User logged out' }
  end

  def failure
    render status: 401, json: { error: 'Login Failed' }
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
