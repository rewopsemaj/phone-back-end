class ApplicationController < ActionController::Base
  def token_authenticate
    authentication_token = params[:auth_token] || request.headers['X-AUTH-TOKEN']
    if authentication_token.blank?
      redirect_to new_user_session_url
    end
    user = User.find_by_authentication_token(authentication_token)
    if user.blank?
      redirect_to new_user_session_url
    end
  end

  # def current_user
  #   authentication_token = params[:auth_token] || request.headers['X-AUTH-TOKEN']
  #   if authentication_token.blank?
  #     return nil
  #   end
  #   user = User.find_by_authentication_token(authentication_token)
  #   if user.blank?
  #     return nil
  #   end
  #   user
  # end
end